// contracts/YESorNOTokenVesting.sol
// SPDX-License-Identifier: MIT
pragma solidity ^0.8.6;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";
import "./YESorNOToken.sol";

contract YESorNOTokenVesting is Ownable, ReentrancyGuard {
    using SafeMath for uint256;

    uint private constant SECOND = 1;
    uint private constant MINUTE = 60 * SECOND;
    uint private constant HOUR   = 60 * MINUTE;
    uint private constant DAY    = 24 * HOUR;

    event Deposit(address indexed sender, uint256 amount);
    event Released(uint256 amount);

    YESorNOToken private immutable _token;
    address      private immutable _beneficiary;
    uint         private immutable _start;
    uint         private immutable _duration;

    // amount already taken by the beneficiary account
    uint256 private _released;

    constructor (YESorNOToken token, address beneficiary, uint delayInDay, uint durationInDay) {
        require(beneficiary != address(0),                   "YESorNOTokenVesting: beneficiary is the zero address");
        require(delayInDay >= 0 && delayInDay <= 3650,       "YESorNOTokenVesting: delayInDay must be more (or equal) than 0 and less than 3651");
        require(durationInDay > 0 && durationInDay <= 3650,  "YESorNOTokenVesting: durationInDay must be more than 0 and less than 3651");

        _token = token;
        _beneficiary = beneficiary;
        _start = block.timestamp.add(delayInDay.mul(DAY));
        _duration = durationInDay.mul(DAY);
    }

    /****************************
     * Getter functions
     ******/

    /**
     * @return the token being held.
     */
    function getToken() public view virtual returns (YESorNOToken) {
        return _token;
    }

    /**
     * @return the beneficiary address.
     */
    function getBeneficiary() public view virtual returns (address) {
        return _beneficiary;
    }

    /**
     * @return the start time of the vesting.
     */
    function getStart() public view virtual returns (uint) {
        return _start;
    }

    /**
     * @return the duration of the vesting.
     */
    function getDuration() public view virtual returns (uint) {
        return _duration;
    }

    /**
     * @return the released amount.
     */
    function getReleased() public view virtual returns (uint256) {
        return _released;
    }

    /**
     * @return the amount available for release.
     */
    function getReleasable() public view virtual returns (uint256) {
        uint256 currentBalance = getCurrentBalance();
        if (currentBalance == 0) {
            return 0;
        }
        
        if (block.timestamp <= _start) {
            return 0;
        } else if (block.timestamp < _start.add(_duration)) {
            return calculateLinearAmountReleasable(currentBalance);
        } else {
            return currentBalance;
        }
    }

    /**
     * @return the amount locked.
     */
    function getLocked() public view virtual returns (uint256) {
        return getCurrentBalance() - getReleasable();
    }

    /**
     * @return the current balance.
     */
    function getCurrentBalance() public view virtual returns (uint256) {
        return _token.balanceOf(address(this));
    }

    /****************************
     * Public functions
     ******/

    /**
     * @notice Transfers available amount to beneficiary.
     */
    function release() public virtual nonReentrant {
        uint256 releasable = getReleasable();
        require(releasable > 0, "YESorNOTokenVesting: no tokens to release");
        _released = _released.add(releasable);
        _token.transfer(_beneficiary, releasable);
        emit Released(releasable);
    }

    /****************************
     * Private functions
     ******/

    /**
     * @return the linear amount releasable.
     */
    function calculateLinearAmountReleasable(uint256 currentBalance) private view returns (uint256) {
        uint256 totalBalance = currentBalance.add(_released);
        uint256 totalBalanceReleasable = totalBalance.mul(block.timestamp.sub(_start)).div(_duration);
        uint256 releasable = totalBalanceReleasable.sub(_released);
        return releasable;
    }

    /****************************
     * External functions
     ******/

    /**
     * @dev Receive token transfers
     */
    receive() external payable {
        emit Deposit(msg.sender, msg.value);
    }
}
