// contracts/YESorNOTokenVestingRevocable.sol
// SPDX-License-Identifier: MIT
pragma solidity ^0.8.6;

import "./YESorNOTokenVesting.sol";

/**
* @notice For example: This contract can be used for an employee. It can be revoked
* by the owner if the employee resigns before the end of the acquisition period.
*/
contract YESorNOTokenVestingRevocable is YESorNOTokenVesting {

    event Revoked();

    bool private _revoked;

    constructor (YESorNOToken token, address beneficiary, uint delayInDay, uint durationInDay) YESorNOTokenVesting(token, beneficiary, delayInDay, durationInDay) {}

    /****************************
     * Getter functions
     ******/

    /**
     * @return the released amount.
     */
    function getRevoked() public view virtual returns (bool) {
        return _revoked;
    }

    /****************************
     * Public functions
     ******/

    /**
     * @notice Allows the owner to revoke the vesting. Tokens already vested
     * remains available for the beneficiary, the rest are burned.
     * 
     * This function can stop the acquisition of tokens for the beneficiary
     * (for example if an employee resigns before the end of the acquisition period).
     * The tokens already acquired remain accessible to the beneficiary with the
     * function release(). The tokens not yet acquired are burned.
     */
    function revoke() public virtual nonReentrant onlyOwner {
        require(_revoked != true, "YESorNOTokenVestingRevocable: is already revoked");

        uint256 currentBalance = getCurrentBalance();
        uint256 releasable = getReleasable();
        uint256 burnable = currentBalance - releasable;

        require(burnable > 0, "YESorNOTokenVestingRevocable: no tokens to burn");

        getToken().burn(burnable);

        _revoked = true;

        emit Revoked();
    }

    /**
     * @return the amount available for release.
     */
    function getReleasable() public view virtual override returns (uint256) {
        if (_revoked == true)  {
            return getCurrentBalance();
        }
        return super.getReleasable();
    }
}